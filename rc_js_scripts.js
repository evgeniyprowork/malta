function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var gdpr = getCookie("gdpr");
    if (gdpr == "true") {
        jQuery('.cookie_message_container').removeClass('show active');
    } else {
        jQuery('.cookie_message_container').addClass('show');
        setTimeout(function () {
            jQuery('.cookie_message_container').addClass('active');
        }, 300);
    }
}

function setCookie() {
    document.cookie = "gdpr=true;";
    checkCookie();
}


jQuery(document).ready(function () {
    checkCookie();


    jQuery('.main-navigation a').on('click', function (e) {

        var get_section = jQuery(this).attr('href');
        if (jQuery(get_section).length) {
            e.preventDefault();
            section_offset = jQuery(get_section).offset().top - 100;
            jQuery('body,html').animate({
                scrollTop: section_offset
            }, 1000);
        } else {
            var target = jQuery(location).attr("hash");
            var offset = (jQuery(this).attr('data-offset') ? jQuery(this).attr('data-offset') : 0);
            jQuery('body,html').animate({
                scrollTop: jQuery(target).offset().top - offset - 100
            }, 700);

            e.preventDefault();
        }

    });

});


jQuery(function ($) {
    $('.rc_loadmore').click(function () {

        var button = $(this),
            data = {
                'action': 'loadmore',
                'query': rc_loadmore_params.posts,
                'page': rc_loadmore_params.current_page
            };

        $.ajax({
            url: rc_loadmore_params.ajaxurl,
            data: data,
            type: 'POST',
            beforeSend: function (xhr) {
                button.text('Loading...');
            },
            success: function (data) {
                if (data) {

                    $("#main-content ").append(data);

                    rc_loadmore_params.current_page++;

                    if (rc_loadmore_params.current_page == rc_loadmore_params.max_page)
                        button.remove();

                    var container = '#main-content .new-posts';
                    $(container).hide();
                    $($(container + ' article')).addClass('post');
                    $($(container + ' .masonry-grid')).each(function () {
                        $items = $(this);
                        console.log($('.masonry-grid').attr('style'));
                        $grid.append($items).masonry('appended', $items);
                    });
                    setTimeout(function () {
                        $grid.masonry('layout');
                    }, 500);

                } else {
                    button.remove(); // if no data, remove the button
                }
            }
        });
    });
});


//const BASE_URL = 'https://wp.rankchecker.io/';
// Register Form
jQuery(document).ready(function () {

	  //Login Form
    jQuery('#loginForm').submit(function (e) {
        const loginFormError = document.querySelector('.loginFormError');
        e.preventDefault();
        jQuery.ajax({
            url: 'https://dev.rankchecker.io/login',
            type: 'POST',
            dataType: 'json',
            headers: {
                'Accept': 'application/json'
            },
            data: jQuery("#loginForm").serialize(),
            success: function (response) {
                console.log(response)
                if (response.redirect_url) {
										loginFormError.style.display = 'none';
                    loginFormError.textContent = '';
                    window.location.href = response.redirect_url;
                }
                /*else {
                  $(this).replaceWith(response.form);
                    }*/
            },
            error: function (error) {
                console.log(error);
                if (error) {
                    loginFormError.textContent = error.responseJSON.message + ' ' + error.responseJSON.errors.email;
                    loginFormError.style.display = 'block';
                }

                console.log('Error status: ', error.status);
                console.log('Error message: ', error.responseJSON.message)
                console.log('Error text: ', error.responseText)
            }
        });

    });

    //Register Form
    jQuery('#registerForm').submit(function (e) {
        e.preventDefault();
				const registerFormError = document.querySelector('.registerFormError');
        const registerFormSuccess = document.querySelector('.registerFormSuccess');
        jQuery.ajax({
            url: 'https://dev.rankchecker.io/register',
            type: 'POST',
            dataType: 'json',
            headers: {
                'Accept': 'application/json'
            },
            data: jQuery("#registerForm").serialize(),
            success: function (response) {
                console.log(response)
								if(response) {

									 registerFormError.textContent = '';
									 if (registerFormSuccess) {
										 registerFormError.classList.remove('alert-danger', 'd-block');
										 registerFormError.classList.add('d-none');

										 registerFormSuccess.classList.add('alert-success', 'd-block');
										 registerFormSuccess.textContent = response.message || 'You have successfully registered';
									 }
								}
                if (response.redirect_url) {
                    window.location.href = response.redirect_url;
                }
                /* else {
                  $(this).replaceWith(response.form);
                    }*/
            },
            error: function (error) {
                console.log(error);
                console.log('Error status: ', error.status);
                console.log('Error message: ', error.responseJSON.message);
                console.log('Error text: ', error.responseText);
				
								if (error) {
										registerFormSuccess.textContent = '';
										if (registerFormError) {
											registerFormSuccess.classList.remove('alert-success', 'd-block');
											registerFormSuccess.classList.add('d-none');

											registerFormError.classList.add('alert-danger', 'd-block');
											registerFormError.textContent = error.responseJSON.message + ' ' +
												(error.responseJSON.errors.email ||
													error.responseJSON.errors.give_approval ||
													error.responseJSON.errors.password);
										}


                }
            }
        });

    });

    //Reset Password STEP 1
    jQuery('#formResetEmail').submit(function (e) {
        e.preventDefault();
        const formResetEmailError = document.querySelector('.formResetEmailError');
        const formResetEmailSuccess = document.querySelector('.formResetEmailSuccess');
        const formResetEmailMessage = document.querySelector('.formResetEmailMessage');
        jQuery.ajax({
            url: 'https://dev.rankchecker.io/password/email',
            type: 'POST',
            dataType: 'json',
            headers: {
                'Accept': 'application/json'
            },
            data: jQuery("#formResetEmail").serialize(),
            success: function (response) {
                console.log(response)
							formResetEmailMessage.textContent = '';

                if (formResetEmailMessage) {
									formResetEmailMessage.classList.remove("alert-danger")
									formResetEmailMessage.classList.add("alert-success")
									formResetEmailMessage.textContent = response.message;
								}
            },
            error: function (error) {
                console.log(error);
                if (error) {
									formResetEmailMessage.textContent = ''

										if (formResetEmailMessage) {
											formResetEmailMessage.classList.remove("alert-success")
											formResetEmailMessage.classList.add("alert-danger")
											formResetEmailMessage.textContent = error.responseJSON.message + ' ' + error.responseJSON.errors.email
										}
                }
                console.log('Error status: ', error.status);
                console.log('Error message: ', error.responseJSON.message)
                console.log('Error text: ', error.responseText)
            }
        });

    });
	
	
	//Reset Password STEP 2
    jQuery('#formResetPassword').submit(function (e) {
        e.preventDefault();
        // const formResetPasswordError = document.querySelector('.formResetPasswordError');
        // const formResetPasswordSuccess = document.querySelector('.formResetPasswordSuccess');
        const formResetPasswordMessage = document.querySelector('.formResetPasswordMessage');
        jQuery.ajax({
            url: 'https://dev.rankchecker.io/password/reset',
            type: 'POST',
            dataType: 'json',
            headers: {
                'Accept': 'application/json'
            },
            data: jQuery("#formResetPassword").serialize(),
            success: function (response) {
                console.log(response)
							const redirectURL = "https://wp.rankchecker.io/log-in/";
							const redirectTime = 3000;
							if (response) {

								if (formResetPasswordMessage) {
									formResetPasswordMessage.textContent = '';
									formResetPasswordMessage.classList.remove("alert-danger")
									formResetPasswordMessage.classList.add("alert-success")
									formResetPasswordMessage.textContent = response.message;
								}

								setTimeout(()=> window.location.href = redirectURL, redirectTime)
							}

                /*if (response.redirect_url) {
                     window.location.href = response.redirect_url;
                  }*/
                /* else {
                  $(this).replaceWith(response.form);
                    }*/
            },
            error: function (error) {
                console.log(error);
                /*if (error) {
                    formResetEmailError.textContent = error.responseJSON.errors.email
                }*/
                console.log('Error status: ', error.status);
                console.log('Error message: ', error.responseJSON.message)
                console.log('Error text: ', error.responseText)

								if (error) {
									if (formResetPasswordMessage) {
										formResetPasswordMessage.textContent = '';
										formResetPasswordMessage.classList.remove("alert-success")
										formResetPasswordMessage.classList.add("alert-danger")
										formResetPasswordMessage.textContent = error.responseJSON.message + ' ' +
											(error.responseJSON.errors.email ||
												error.responseJSON.errors.password)
									}

								}
            }
        });

    });	
	

    // Checker
    jQuery('#main-form').submit(function (e) {
        e.preventDefault();
        // const mainFormSuccess = document.querySelector('.main-form-success');
        // const mainFormError = document.querySelector('.main-form-error');
        const mainFormMessage = document.querySelector('.mainFormMessage');


        jQuery.ajax({
            url: 'https://dev.rankchecker.io/api/v1/checker',
            type: 'POST',
            dataType: 'json',
            headers: {
                'Accept': 'application/json'
            },
            data: jQuery("#main-form").serialize(),
            success: function (response) {
                console.log(response)
                if (response) {
									if (mainFormMessage) {
										mainFormMessage.textContent = ''
										mainFormMessage.classList.remove("alert-danger")
										mainFormMessage.classList.add("alert-success")
										mainFormMessage.textContent = 'We have e-mailed your checker link!'
									}
                }
                /* else {
                  $(this).replaceWith(response.form);
                    }*/
            },
            error: function (error) {
                console.log(error);
                console.log('Error status: ', error.status);
                console.log('Error message: ', error.responseJSON.message)
                console.log('Error text: ', error.responseText)

                if (error) {
                	if (mainFormMessage) {
										mainFormMessage.textContent = ''
										mainFormMessage.classList.remove("alert-success")
										mainFormMessage.classList.add("alert-danger")
										mainFormMessage.textContent = error.responseJSON.message
									}

                }
            }
        });

    });

});

