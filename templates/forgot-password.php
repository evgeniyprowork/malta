<?php
/*
Template Name: Forgot Password Page
Template Post Type: page
*/

get_header();

?>
    <section class="wrapper wrap-detail-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content' ); ?>>
                        <div class="post-content-inner">
                            <div class="post-text">

                                <div class="wp-block-group center-title login-container">
                                    <div class="wp-block-group__inner-container">
                                        <h1 class="center-bar">Forgot our password ?</h1>


                                        <p class="has-regular-font-size"><strong>Enter your email address.</strong> An email will be sent to you with more information.</p>


                                        <form method="POST" id="formResetEmail" name="formResetEmail" action="https://rankchecker.io/password/email" onsubmit="resetPreventDef(event)">
                                            
                                            <!--<div class="formResetEmailResponse">
                                                <span class="formResetEmailError"></span>
                                                <span class="formResetEmailSuccess"></span>
                                            </div>-->
											<!--<div class="col-12 col-md-6 offset-md-3">
												<div class="formResetEmailError alert alert-danger d-none" role="alert"></div>
												<div class="formResetEmailSuccess alert alert-success d-none" role="alert"></div>
											</div>-->
											<div class="col-12 col-md-6 offset-md-3">
												<div class="formResetEmailMessage alert" role="alert"></div>
											</div>

                                            <div class="form-group">
                                                <div class="col-12 col-md-6 offset-md-3">
                                                    <input id="email" type="email" class="form-control " name="email" value="" required="" autocomplete="email" autofocus="">

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-12 text-center">
                                                    <button type="submit" class="formResetEmailGap btn btn-lg btn-success px-5">
                                                        Send recovery link
                                                    </button>
                                                </div>
                                            </div>
                                            <p class="form-register-message-success"></p>
                                        </form>
                                    </div>
                                </div>

                            </div>
							<?php multiple_business_entry_footer(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();