<?php
/*
Template Name: Reset Password Page
Template Post Type: page
*/

get_header();

if (empty($_GET['token']) || empty($_GET['email'])) {
    wp_redirect('/forgot-password');
    exit;
}

?>
    <section class="wrapper wrap-detail-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content' ); ?>>
                        <div class="post-content-inner">
                            <div class="post-text">


                                <div class="wp-block-group center-title login-container">
                                    <div class="wp-block-group__inner-container">
                                        <h1 class="center-bar">Reset Password</h1>
                                        <form method="POST" id="formResetPassword" action="">

                                            <input type="hidden" name="token" value="<?= sanitize_text_field($_GET['token']) ?>">

                                            <div class="form-group">
                                                <div class="col-12 col-md-6 offset-md-3">
                                                    <input id="email" type="email" class="form-control " name="email" value="<?= sanitize_text_field($_GET['email']) ?>" required readonly placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-12 col-md-6 offset-md-3">
                                                    <input id="password" type="password" class="form-control " name="password" required="" autocomplete="new-password" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-12 col-md-6 offset-md-3">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required="" autocomplete="new-password" placeholder="Confirm password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-12 text-center">
                                                    <button type="submit" class="btn btn-lg btn-success px-5 ">
                                                        Reset Password
                                                    </button>

<!--                                                    <div class="formResetResponse">-->
<!--                                                        <span class="formResetPasswordError"></span>-->
<!--                                                        <span class="formResetPasswordSuccess"></span>-->
<!--                                                    </div>-->

                                                </div>
                                            </div>
											<div class="col-12 col-md-6 offset-md-3 my-3">
												<div class="formResetPasswordMessage alert" role="alert"></div>
											</div>
                                        </form>
                                    </div>
                                </div>


                            </div>
							<?php multiple_business_entry_footer(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();