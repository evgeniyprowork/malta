<?php
/*
Template Name: Checker Result
Template Post Type: page
*/

get_header();

?>
    <section class="wrapper wrap-detail-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content' ); ?>>
                        <div class="post-content-inner">
                            <div class="post-text">

                                <div class="wp-block-group center-title">
                                    <div class="wp-block-group__inner-container">

										<div class="row">
											<div class="col-12 col-lg-12 mb-3 text-left">
												<h2 class="section__caption mb-3">Thank you for using Rankchecker.io service</h2>
											</div>

											<div class="col-12">
												<div class="table-responsive-lg">
													<table class="keyword-result__table table">
														<thead>
														<tr>
															<th class="sorted sorted--asc" scope="col">Keyword <i class="fas fa-caret-up" aria-hidden="true"></i></th>
															<th scope="col">Device</th>
															<th scope="col">Keyword Position</th>
															<th scope="col">Search Engine</th>
															<th class="text-center" scope="col">URL</th>
														</tr>
														</thead>
														<tbody>
														<tr>
															<th scope="row">test</th>
															<td class="text-center">
																<i class="fas fa-desktop" aria-hidden="true"></i>
																<small>desktop</small>
															</td>
															<td>
																Not in the Top 100
															</td>
															<td>google.com</td>
															<td><a href="//academweb.com" target="_blank">academweb.com</a></td>
														</tr>
														</tbody>
													</table>
												</div>

												<div class="keyword-result__info bg-success text-center text-white py-2">
													<span>Report created 1 day ago for website academweb.com</span>
												</div>
											</div>

											<div class="col-lg-8  col-md-12 pb-3 pt-3 mx-auto">
												<div class="container">
													<div class="row align-items-center checker_block">
														<h2 class="col-12 text-center"> Save your time and check several keywords at once wit our keyword rank checker tool.</h2>
														<p class="col-12 text-center p-2">
															Create a free user and check up to 50 keywords at once. Account creation gives you the benefit of having up to 5 target websites.
														</p>
														<div class="col-12 text-center align-self-center p-2">
															<a href="/register" class="btn btn-success btn-lg text-white">Create free user</a>
														</div>
													</div><!-- /.row -->
												</div><!-- /.container -->
											</div><!-- /cta__register-section -->

										</div>
										<div id="updates-section" class="section text-center py-5 bg-secondary">
											<div class="container">
												<div class="row">
													<div class="col-12 col-md-8 mx-auto mb-5">
														<h2 class="section__title mb-4">Get updates of new services</h2>
														<p>
															Don’t miss out on new tools! Rankchecker.io is constantly developing free SEO tools.
															We will be releasing more functions and adding more features in the next few months.
															Sign up to get updates on new services and tools, as well as the latest SEO tips and news!
														</p>
													</div>

													<div class="col-12 col-md-6 mx-auto">
														<form method="post" action="https://dev.rankchecker.io/newsletter">
															<input type="hidden" name="_token" value="QUj7XzJs3ePOLtlPrv8RXIjFYcldcFojtiebTemI">                            <div class="input-group mb-4">
																<input required="" type="email" class="form-control" id="email" name="email" placeholder="Email">
															</div>

															<div>
																<button type="submit" class="btn btn-success btn-lg btn-block">Subscribe</button>
															</div>
														</form>

													</div>

												</div><!-- /.row -->
											</div><!-- /.container -->
										</div>

                                    </div>
                                </div>

                            </div>
							<?php multiple_business_entry_footer(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();