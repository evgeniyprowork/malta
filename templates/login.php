<?php
/*
Template Name: Login Page
Template Post Type: page
*/

get_header();

?>
    <section class="wrapper wrap-detail-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content' ); ?>>
                        <div class="post-content-inner">
                            <div class="post-text">

                                <div class="wp-block-group center-title login-container">
                                    <div class="wp-block-group__inner-container">

                                        <h1 class="center-bar">Log In</h1>

                                        <p class="section__text text-center">Don’t have an account yet. <a href="https://wp.rankchecker.io/register/" data-type="page" data-id="596">Sign up</a>!</p>

                                        <div class="section__block mb-3 pt-3">
                                            <form id="loginForm" name="loginForm" action="https://rankchecker.io/login" method="POST" onsubmit="loginPreventDef(event)">
                                                <!--<span class="loginFormError"></span>-->
												<div class="row">
													<div class="col-12 col-md-6 mx-auto">
														<div class="loginFormError alert alert-danger" role="alert"></div>
													</div>
												</div>


                                                <div class="form-group row">
                                                    <div class="col-12 col-md-6 mx-auto"><input id="emailAddress" class="form-control " autocomplete="email" name="email" required="" type="email" value="" autofocus="" placeholder="Email"></div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-12 col-md-6 mx-auto"><input id="password" class="form-control " autocomplete="current-password" name="password" required="" type="password" placeholder="Password"></div>
                                                </div>

                                                <!--<div class="form-group row">-->
                                                <!--    <div class="col-12 col-md-6 mx-auto">-->
                                                <!--        <div class="g-recaptcha" data-sitekey="6LeJDsAUAAAAAHWKWE9FRlcp2ruwnEolOJsraeN1">-->
                                                <!--            <div style="width: 304px; height: 78px;">-->
                                                <!--                <div>-->
                                                <!--                    <iframe loading="lazy" title="reCAPTCHA" role="presentation" src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LeJDsAUAAAAAHWKWE9FRlcp2ruwnEolOJsraeN1&amp;co=aHR0cHM6Ly9yYW5rY2hlY2tlci5pbzo0NDM.&amp;hl=en&amp;v=f-bnnOuahiYKuei7dmAd3kgv&amp;size=normal&amp;cb=8bnklve4b76w" name="a-1glg4yumpzpq" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox allow-storage-access-by-user-activation" width="304" height="78" frameborder="0"></iframe>-->
                                                <!--                </div>-->
                                                <!--                <textarea id="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none; display: none;" name="g-recaptcha-response"></textarea></div>-->
                                                <!--            <iframe style="display: none;"></iframe>-->
                                                <!--        </div>-->
                                                <!--    </div>-->
                                                <!--</div>-->

                                                <div class="form-group row mb-0">
                                                    <div class="col-md-8 mx-auto text-center">
                                                        <button class="btn btn-lg btn-success px-5" type="submit"> Log In</button>
                                                    </div>
                                                    <div class="col-md-8 mx-auto text-center"><a class="btn btn-link font-weight-bold" href="/forgot-password"> Forgot Your Password? </a></div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="section__block border-top pt-3">
                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 mx-auto text-center mb-2"><a class="btn btn-lg login-button login-button--facebook" href="https://rankchecker.io/login/facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i> Log in with Facebook</a></div>
                                                <div class="col-md-8 mx-auto text-center"><a class="btn btn-lg login-button login-button--google" href="https://rankchecker.io/login/google"><i class="fab fa-google-plus-g" aria-hidden="true"></i> Log in with Google</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
							<?php multiple_business_entry_footer(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();