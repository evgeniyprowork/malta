<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @since Multiple Business 1.0.0
 */
get_header();

?>
<section class="wrapper wrap-detail-page">

<?php if ( is_front_page() ) : ?>
	<div class="font-page-content-wrapper" >
		<div class="">
			<div class="">


<?php
else :
?>
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
<?php
endif;
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/page/content', '' );

					# If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; # End of the loop.
?>				
			</div>
		</div>
	</div>
</section>


<?php
global $post;
if ( has_blocks( $post->post_content ) ) {
	$blocks = parse_blocks( $post->post_content );
	$faq_content = [];
	$faq_question = [];
	foreach ($blocks as $block) {
		if(strpos($block['attrs']['className'], 'faq-container' ) != false || $block['attrs']['className'] == 'faq-container' ){
			$html =  render_block($block);
			$classname = 'uagb-faq-content';
			$dom = new DOMDocument('1.0', 'UTF-8');
			$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
			$xpath = new DOMXPath($dom);
			$results = $xpath->query("//div[@class='". $classname ."']");
			$i=0;
			foreach ($results as $result) {  
				$faq_content[$i] = $result->nodeValue;
				$i++;
			}
			$results = $xpath->query("//span[@class='uagb-question']");
			$i=0;
			foreach ($results as $result) {  
				$faq_question[$i] = $result->nodeValue;
				$i++;
			}      
			$length = $i;
			if($faq_content && $faq_question ){
				?>
				<script type="application/ld+json">
				{
				"@context": "https://schema.org",
				"@type": "FAQPage",
				"mainEntity": [
				<?php
				for($i=0; $i< $length ; $i++){
					?>
				  {
				"@type": "Question",
				"name": "<?php echo $faq_question[$i]; ?>",
				"acceptedAnswer": {
				  "@type": "Answer",
				  "text": "<p><?php echo $faq_content[$i]; ?></p>"
				}
				<?php
				if($i == $length-1){
				  echo '}';
				}
				else{
				  echo '},';
				}
				}
				?>
				]
				}
				</script>
				<?php
			}
		} else if ( strpos($block['attrs']['className'], 'review-container' ) || $block['attrs']['className'] == 'review-container' ){
		?>
			<script type="application/ld+json">
			{
			"@context": "https://schema.org/",
			"@type": "AggregateRating",
			"itemReviewed": {
			"@type": "Organization",
			"image": "<?php echo get_home_url(); ?>/wp-content/uploads/2021/06/rc_og.jpg",
			"name": "Rankchecker.io"
			},
			"ratingValue": "90",
			"bestRating": "100",
			"ratingCount": "56"
			}
			</script>   
		<?php
		}
	} 
}

get_footer();